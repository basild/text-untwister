/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basildsouza.textuntwister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Basil Dsouza
 */
public class Untwister {

    public static final boolean DEBUG = true;
    private final List<String> words;
    private final String dictionary;
    
    private static Map<String, List<String>> dictionaryStore = new HashMap(5);

    public Untwister(final String dictionary) throws IOException {
        this.dictionary = dictionary;
        words = dictionaryStore.get(dictionary);
    }
    
    public static List<String> loadDictionary(final String dictionary) throws IOException{
        List<String> tempWordList = dictionaryStore.get(dictionary);
        if(tempWordList != null){
            return tempWordList;
        }
        
        tempWordList = new LinkedList();
        InputStream fileStream = Untwister.class.getResourceAsStream("/" + dictionary);
        if(fileStream == null){
            throw new IOException("Could not find resource: " + "/" + dictionary);
        }

        long time = System.currentTimeMillis();
        System.out.println("Loading: ");
        final BufferedReader fileIn = new BufferedReader(new InputStreamReader(fileStream));
        String tempString = fileIn.readLine();
        while(tempString != null){
            tempWordList.add(tempString);
            tempString = fileIn.readLine();
        }
        System.out.println("Finished: " + (System.currentTimeMillis()-time));
        
        dictionaryStore.put(dictionary, tempWordList);
        
        return tempWordList;
    }

    public List<String> untwistWords(final String word) throws IOException {
        String tempString;
        char uniqueSet[];
        int uniqueCount[];
        final ArrayList<String> wordList = new ArrayList();

        tempString = "";
        for (int i = 0; i < word.length(); i++) {
            if (tempString.indexOf(word.charAt(i)) == -1) {
                tempString += word.charAt(i);
            }
        }

        uniqueSet = tempString.toCharArray();
        uniqueCount = new int[uniqueSet.length];
        for (int i = 0; i < tempString.length(); i++) {
            uniqueCount[i] = wordCount(word, uniqueSet[i]);
        }

        
        if(words != null){
            for(String currWord : words){
                processWord(currWord, wordList, uniqueSet, uniqueCount);
            }
        } else {
            InputStream fileStream = Untwister.class.getResourceAsStream("/" + dictionary);
            if(fileStream == null){
                throw new IOException("Could not find resource: " + "/" + dictionary);
            }
            
            final BufferedReader fileIn = new BufferedReader(new InputStreamReader(fileStream));
            String currWord = fileIn.readLine();
            while(currWord != null){
                processWord(currWord, wordList, uniqueSet, uniqueCount);
                currWord = fileIn.readLine();
            }
        }
        
        return wordList;
    }

    private static int wordCount(final String toCount, final char toMatch) {
        int wordCounts = 0;

        for (int i = 0; i < toCount.length(); i++) {
            if (toCount.charAt(i) == toMatch) {
                wordCounts++;
            }
        }

        return wordCounts;
    }

    private void processWord(final String currWord, final ArrayList<String> wordList, 
                             char[] uniqueSet,  int[] uniqueCount) {
        int singleWords;
        int temp;
        int i;

        singleWords = 0;
        for (i = 0; i < uniqueSet.length; i++) {
            temp = wordCount(currWord, uniqueSet[i]);
            if (!((uniqueCount[i] >= temp))) {
                break;
            }
            singleWords += temp;
        }
        if (i == uniqueSet.length && currWord.length() == singleWords) {
            wordList.add(currWord);
        }
    }
}
