/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basildsouza.textuntwister;

/**
 *
 * @author Basil Dsouza
 */
public class UntwistingLifeCycleManager {

    /** 
     * Stores the status of the untwisting.
     * <p>
     * It is marked volatile to avoid any problems with syncronization. 
     * Since only the pause button will write to it, and all others will
     * only read.
     */
    private boolean paused;
    /**
     * Syncronizing object for the start thread to wait on.
     */
    private final Object pauseObject = new Object();
    private boolean stopped;

    public boolean isPaused() {
        synchronized (pauseObject) {
            return paused;
        }
    }

    public void pause(final boolean shouldPause) {
        synchronized (pauseObject) {
            paused = shouldPause;
            if (!paused) {
                pauseObject.notifyAll();
            }
        }
    }

    public void waitTillUnpaused() throws InterruptedException {
        synchronized (pauseObject) {
            while (paused && !stopped) {
                pauseObject.wait();
            }
        }
    }

    public boolean isStopped() {
        synchronized (pauseObject) {
            return stopped;
        }
    }

    public void stop(final boolean stopped) {
        synchronized (pauseObject) {
            this.stopped = stopped;
            if (stopped) {
                pauseObject.notifyAll();
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        stop(true);
    }
}
