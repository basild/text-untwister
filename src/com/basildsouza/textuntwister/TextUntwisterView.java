/*
 * TextUntwisterView.java
 */

package com.basildsouza.textuntwister;

import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * The application's main frame.
 */
public class TextUntwisterView extends FrameView {
    public static final String BIG_DICTIONARY = "BigDictionary.txt";
    public static final String SMALL_DICTIONARY = "SmallDictionary.txt";
    
    private TaskMonitor taskMonitor;
    private UntwistingLifeCycleManager control;
    
    private static final Logger logger = Logger.getLogger(TextUntwisterView.class.getName());
    private boolean windowFocus;
    
    public TextUntwisterView(final SingleFrameApplication app) {
        super(app);
        
        initComponents();
        
        // status bar initialization - message timeout, idle icon and busy animation, etc
        final ResourceMap resourceMap = getResourceMap();
        final int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        final int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);
        
        //@note: My changes start here
        initTaskMonitor();
        this.getFrame().setResizable(false);
        
        dictButtonGroup.add(conciseRadioButton);
        dictButtonGroup.add(entireRadioButton);
        
        this.getApplication().getContext().getTaskService().execute(new Task<Object, Void>(this.getApplication()) {

            @Override
            protected Object doInBackground() throws Exception {
                Thread.sleep(2000);
                setMessage("Loading Large Dictionary...");
                Untwister.loadDictionary(BIG_DICTIONARY);
                setMessage("Loading Small Dictionary...");
                Untwister.loadDictionary(SMALL_DICTIONARY);
                setMessage("Done Loading Dictionaries.");
                return null;
            }
        });

        this.getFrame().addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            @Override
            public void windowGainedFocus(final WindowEvent e) {
                windowFocus = true;
                if(!pauseToggleButton.isEnabled()){
                    return;
                }
                pauseToggleButton.setSelected(true);
                pauseTyping();
            }

            @Override
            public void windowLostFocus(final WindowEvent e) {
                windowFocus = false;
                /**
                 * is commented to prevent unpausing when focused moved away
                if(!pauseToggleButton.isEnabled()){
                    return;
                }
                pauseToggleButton.setSelected(false);
                pauseTyping();
                 */
            }
        });
    }
    
    private void initTaskMonitor(){
        // connecting action tasks to status bar via TaskMonitor
        taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(final java.beans.PropertyChangeEvent evt) {
                final String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    final String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    final int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }


    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            final JFrame mainFrame = TextUntwisterApp.getApplication().getMainFrame();
            aboutBox = new TextUntwisterAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        TextUntwisterApp.getApplication().show(aboutBox);
    }

    @Action
    public void showHelpDialog() {
        if (helpBox == null) {
            final JFrame mainFrame = TextUntwisterApp.getApplication().getMainFrame();
            helpBox = new TextUntwistUsageDialog(mainFrame, true);
            helpBox.setLocationRelativeTo(mainFrame);
        }
        TextUntwisterApp.getApplication().show(helpBox);
    }

    @Action
    public void genWords() {
        long time = System.currentTimeMillis();
        System.out.println("Gen Words: ");
        generateWords();
        System.out.println("End Gen Words: " + (System.currentTimeMillis()-time));
    }

    @Action
    public void pauseTyping() {
        control.pause(pauseToggleButton.isSelected());
    }

    @Action
    public void stopTyping() {
        control.stop(true);
    }

    @Action
    public Task startTyping() {
        final List<String> words = generateWords();
        control = new UntwistingLifeCycleManager();
        int wordDelay = AutoFiller.DEFAULT_WORD_DELAY;
        try {
            wordDelay  = Integer.parseInt(delayTextField.getText());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog
                    (this.getFrame(), "Value in the delay box in invalid. " +
                     "Default value of " + AutoFiller.DEFAULT_WORD_DELAY + 
                     "ms is being used.", "Invalid delay value", JOptionPane.WARNING_MESSAGE);
        }
        return new UntwisterTyperTask(this, control, words, wordDelay);
    }

    @Action
    public Task startGenTyping() {
        control = new UntwistingLifeCycleManager();
        int wordDelay = AutoFiller.DEFAULT_WORD_DELAY;
        try {
            wordDelay  = Integer.parseInt(delayTextField.getText());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog
                    (this.getFrame(), "Value in the delay box in invalid. " +
                     "Default value of " + AutoFiller.DEFAULT_WORD_DELAY + 
                     "ms is being used.", "Invalid delay value", JOptionPane.WARNING_MESSAGE);
        }
        
        return new UntwisterTyperTask(this, control, 
                                      Arrays.asList(genWordTextArea.getText().split("\n")), 
                                      wordDelay);
    }
    
    private List<String> generateWords(){
        final String dictionary 
                  = entireRadioButton.isSelected()?BIG_DICTIONARY:SMALL_DICTIONARY;
        
        genWordTextArea.setText("");
        final List<String> wordList;
        try {
            wordList = new Untwister(dictionary).untwistWords(scrambledTextField.getText().toLowerCase());
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Could not read dictionary", ex);
            JOptionPane.showMessageDialog
                    (this.getFrame(), "Could not untwist word. An error while " +
                     "reading from the dictionary.", "Could not Untwist word", 
                     JOptionPane.ERROR_MESSAGE);
            return Collections.emptyList();
        }
        final StringBuilder sb = new StringBuilder(wordList.size() * 10);
        for(String word : wordList){
            sb.append(word).append("\n");
        }
        genWordTextArea.setText(sb.toString());
        
        return wordList;
    }
    
    public void setStartButtonStatus(final boolean enabled){
        startButton.setEnabled(enabled);
        genStartButton.setEnabled(enabled);

        if(enabled){
            pauseToggleButton.setSelected(false);
        }
        genPauseToggleButton.setEnabled(!enabled);
        pauseToggleButton.setEnabled(!enabled);
        stopButton.setEnabled(!enabled);
        genStopButton.setEnabled(!enabled);
    }

    public boolean isWindowFocused() {
        return windowFocus;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        mainPanel = new javax.swing.JPanel();
        inputPanel = new javax.swing.JPanel();
        scrambledTextField = new javax.swing.JTextField();
        scrambledLabel = new javax.swing.JLabel();
        dictionaryLabel = new javax.swing.JLabel();
        conciseRadioButton = new javax.swing.JRadioButton();
        entireRadioButton = new javax.swing.JRadioButton();
        startButton = new javax.swing.JButton();
        onlyGenButton = new javax.swing.JButton();
        delayLabel = new javax.swing.JLabel();
        delayTextField = new javax.swing.JTextField();
        delayUnitLabel = new javax.swing.JLabel();
        stopButton = new javax.swing.JButton();
        pauseToggleButton = new javax.swing.JToggleButton();
        outputPanel = new javax.swing.JPanel();
        genWordScrollPane = new javax.swing.JScrollPane();
        genWordTextArea = new javax.swing.JTextArea();
        genStartButton = new javax.swing.JButton();
        genStopButton = new javax.swing.JButton();
        genPauseToggleButton = new javax.swing.JToggleButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        usageMenuItem = new javax.swing.JMenuItem();
        helpSeparator = new javax.swing.JSeparator();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();
        dictButtonGroup = new javax.swing.ButtonGroup();

        mainPanel.setName("mainPanel"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.basildsouza.textuntwister.TextUntwisterApp.class).getContext().getResourceMap(TextUntwisterView.class);
        inputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("inputPanel.border.title"))); // NOI18N
        inputPanel.setName("inputPanel"); // NOI18N

        scrambledTextField.setText(resourceMap.getString("scrambledTextField.text")); // NOI18N
        scrambledTextField.setName("scrambledTextField"); // NOI18N

        scrambledLabel.setText(resourceMap.getString("scrambledLabel.text")); // NOI18N
        scrambledLabel.setName("scrambledLabel"); // NOI18N

        dictionaryLabel.setText(resourceMap.getString("dictionaryLabel.text")); // NOI18N
        dictionaryLabel.setName("dictionaryLabel"); // NOI18N

        conciseRadioButton.setSelected(true);
        conciseRadioButton.setText(resourceMap.getString("conciseRadioButton.text")); // NOI18N
        conciseRadioButton.setToolTipText(resourceMap.getString("conciseRadioButton.toolTipText")); // NOI18N
        conciseRadioButton.setName("conciseRadioButton"); // NOI18N

        entireRadioButton.setText(resourceMap.getString("entireRadioButton.text")); // NOI18N
        entireRadioButton.setName("entireRadioButton"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.basildsouza.textuntwister.TextUntwisterApp.class).getContext().getActionMap(TextUntwisterView.class, this);
        startButton.setAction(actionMap.get("startTyping")); // NOI18N
        startButton.setName("startButton"); // NOI18N

        onlyGenButton.setAction(actionMap.get("genWords")); // NOI18N
        onlyGenButton.setText(resourceMap.getString("onlyGenButton.text")); // NOI18N
        onlyGenButton.setName("onlyGenButton"); // NOI18N

        delayLabel.setText(resourceMap.getString("delayLabel.text")); // NOI18N
        delayLabel.setName("delayLabel"); // NOI18N

        delayTextField.setText(resourceMap.getString("delayTextField.text")); // NOI18N
        delayTextField.setName("delayTextField"); // NOI18N

        delayUnitLabel.setText(resourceMap.getString("delayUnitLabel.text")); // NOI18N
        delayUnitLabel.setName("delayUnitLabel"); // NOI18N

        stopButton.setAction(actionMap.get("stopTyping")); // NOI18N
        stopButton.setText(resourceMap.getString("stopButton.text")); // NOI18N
        stopButton.setEnabled(false);
        stopButton.setName("stopButton"); // NOI18N

        pauseToggleButton.setAction(actionMap.get("pauseTyping")); // NOI18N
        pauseToggleButton.setText(resourceMap.getString("pauseToggleButton.text")); // NOI18N
        pauseToggleButton.setEnabled(false);
        pauseToggleButton.setName("pauseToggleButton"); // NOI18N

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, genPauseToggleButton, org.jdesktop.beansbinding.ELProperty.create("${selected}"), pauseToggleButton, org.jdesktop.beansbinding.BeanProperty.create("selected"));
        bindingGroup.addBinding(binding);

        javax.swing.GroupLayout inputPanelLayout = new javax.swing.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(scrambledLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrambledTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(startButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pauseToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(stopButton))
                    .addComponent(onlyGenButton, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dictionaryLabel)
                    .addComponent(delayLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(entireRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(conciseRadioButton)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(delayTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(delayUnitLabel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        inputPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {pauseToggleButton, startButton, stopButton});

        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scrambledLabel)
                            .addComponent(scrambledTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(startButton)
                            .addComponent(stopButton)
                            .addComponent(pauseToggleButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(onlyGenButton))
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(conciseRadioButton)
                            .addComponent(dictionaryLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(entireRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(delayTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(delayUnitLabel))
                            .addComponent(delayLabel))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        outputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("outputPanel.border.title"))); // NOI18N
        outputPanel.setName("outputPanel"); // NOI18N

        genWordScrollPane.setName("genWordScrollPane"); // NOI18N

        genWordTextArea.setColumns(20);
        genWordTextArea.setRows(5);
        genWordTextArea.setName("genWordTextArea"); // NOI18N
        genWordScrollPane.setViewportView(genWordTextArea);

        genStartButton.setAction(actionMap.get("startGenTyping")); // NOI18N
        genStartButton.setName("genStartButton"); // NOI18N

        genStopButton.setAction(actionMap.get("stopTyping")); // NOI18N
        genStopButton.setText(resourceMap.getString("genStopButton.text")); // NOI18N
        genStopButton.setEnabled(false);
        genStopButton.setName("genStopButton"); // NOI18N

        genPauseToggleButton.setAction(actionMap.get("pauseTyping")); // NOI18N
        genPauseToggleButton.setText(resourceMap.getString("genPauseToggleButton.text")); // NOI18N
        genPauseToggleButton.setEnabled(false);
        genPauseToggleButton.setName("genPauseToggleButton"); // NOI18N

        javax.swing.GroupLayout outputPanelLayout = new javax.swing.GroupLayout(outputPanel);
        outputPanel.setLayout(outputPanelLayout);
        outputPanelLayout.setHorizontalGroup(
            outputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(outputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(outputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(genWordScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, outputPanelLayout.createSequentialGroup()
                        .addComponent(genStartButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(genPauseToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(genStopButton)))
                .addContainerGap())
        );

        outputPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {genPauseToggleButton, genStartButton, genStopButton});

        outputPanelLayout.setVerticalGroup(
            outputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(outputPanelLayout.createSequentialGroup()
                .addComponent(genWordScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(outputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(genStartButton)
                    .addComponent(genStopButton)
                    .addComponent(genPauseToggleButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(outputPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(inputPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(outputPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        usageMenuItem.setAction(actionMap.get("showHelpDialog")); // NOI18N
        usageMenuItem.setText(resourceMap.getString("usageMenuItem.text")); // NOI18N
        usageMenuItem.setName("usageMenuItem"); // NOI18N
        helpMenu.add(usageMenuItem);

        helpSeparator.setName("helpSeparator"); // NOI18N
        helpMenu.add(helpSeparator);

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 242, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton conciseRadioButton;
    private javax.swing.JLabel delayLabel;
    private javax.swing.JTextField delayTextField;
    private javax.swing.JLabel delayUnitLabel;
    private javax.swing.ButtonGroup dictButtonGroup;
    private javax.swing.JLabel dictionaryLabel;
    private javax.swing.JRadioButton entireRadioButton;
    private javax.swing.JToggleButton genPauseToggleButton;
    private javax.swing.JButton genStartButton;
    private javax.swing.JButton genStopButton;
    private javax.swing.JScrollPane genWordScrollPane;
    private javax.swing.JTextArea genWordTextArea;
    private javax.swing.JSeparator helpSeparator;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton onlyGenButton;
    private javax.swing.JPanel outputPanel;
    private javax.swing.JToggleButton pauseToggleButton;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel scrambledLabel;
    private javax.swing.JTextField scrambledTextField;
    private javax.swing.JButton startButton;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JButton stopButton;
    private javax.swing.JMenuItem usageMenuItem;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;
    private JDialog helpBox;
}
