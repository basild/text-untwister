package com.basildsouza.textuntwister;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.application.Task;

class UntwisterTyperTask extends Task<Object, Void> {
    private final List<String> words;
    private final UntwistingLifeCycleManager control;
    private final AutoFiller filler;
    private final TextUntwisterView view;
    
    private static final String UNTWIST_STATUS_FORMAT = "Untwisting [%3d of %3d]%-4s";
    private static final Logger logger = Logger.getLogger(UntwisterTyperTask.class.getName());

    public UntwisterTyperTask(final TextUntwisterView view,
            final UntwistingLifeCycleManager control,
            final List<String> words, final int wordDelay) {
        // Runs on the EDT.  Copy GUI state that
        // doInBackground() depends on from parameters
        // to StartTypingTask fields, here.
        super(view.getApplication());

        this.view = view;
        this.words = words;
        this.control = control;
        this.filler = new AutoFiller(wordDelay);
        view.setStartButtonStatus(false);
    }

    @Override
    protected Object doInBackground() {
        boolean wasPaused = false;
        final int totalSize = words.size();
        int currWordLoc = 0;
        String[] dots = new String[]{"", ".", "..", "..."};
        int dotPos = 0;

        if (view.isWindowFocused()) {
            filler.switchApplication();
        }
        for (String currWord : words) {
            while (control.isPaused() && !control.isStopped()) {
                wasPaused = true;
                try {
                    setMessage("Paused Untwisting");
                    control.waitTillUnpaused();
                } catch (InterruptedException ex) {
                    logger.log(Level.SEVERE,
                            "Unexpected interuption while waiting for unpause.");
                    return null;
                }
            }
            if (control.isStopped()) {
                setMessage("Stopped Untwisting");
                return null;
            } else if (wasPaused) {
                if (view.isWindowFocused()) {
                    filler.switchApplication();
                }
                wasPaused = false;
            }

            filler.typeWord(currWord);

            setProgress(currWordLoc, 0, totalSize);
            setMessage(String.format(UNTWIST_STATUS_FORMAT, currWordLoc+1, totalSize, dots[dotPos]));
            dotPos = (dotPos + 1) % dots.length;
            currWordLoc++;
        }
        if (!view.isWindowFocused()) {
            filler.switchApplication();
        }

        setMessage("Done Untwisting");
        return null;
    }

    @Override
    protected void finished() {
        super.finished();
        view.setStartButtonStatus(true);
    }
}
