/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basildsouza.textuntwister;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Basil Dsouza
 */
public class AutoFiller {

    public static final int DEFAULT_KEY_DELAY = 100;
    public static final int DEFAULT_WORD_DELAY = 500;
    private final Robot robot;
    private int wordDelay;
    private static final Logger logger = Logger.getLogger(AutoFiller.class.getName());

    public AutoFiller() {
        this(DEFAULT_WORD_DELAY);
    }

    public AutoFiller(final int wordDelay) {
        try {
            robot = new Robot();
            this.wordDelay = wordDelay;
        } catch (AWTException ex) {
            logger.log(Level.SEVERE, "Could not create robot", ex);
            logger.log(Level.SEVERE, "Unexpected error: Cannot continue.");
            throw new IllegalStateException
                    ("Unexpected error. Cannot proceed without creating robot instance.");
        }
    }

    public void switchApplication() {
        switchApplication(1);
    }

    public void switchApplication(final int away) {
        for (int i = 0; i < away; i++) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_TAB);
            
            robot.delay(DEFAULT_KEY_DELAY);
            
            robot.keyRelease(KeyEvent.VK_TAB);
            robot.keyRelease(KeyEvent.VK_ALT);
        }
    }

    public void typeWord(final String word) {
        for (int i = 0; i < word.length(); i++) {
            keyType(Character.getNumericValue(word.charAt(i)) + 55);
        }
        keyType(KeyEvent.VK_ENTER);
        robot.delay(wordDelay);
    }

    private void keyType(final int keyStroke) {
        robot.keyPress(keyStroke);
        robot.delay(DEFAULT_KEY_DELAY);
        //System.out.println("Typing keyStore: "  + keyStroke);
        robot.keyRelease(keyStroke);
    }
}
