/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ToDel;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author Basil Dsouza
 */
public class AudioCapture extends Thread {
    private TargetDataLine dataLine;
    private AudioFileFormat.Type audioFileType;
    private AudioInputStream incomingStream;
    private File generatedFile;

    public AudioCapture(TargetDataLine line, 
                        AudioFileFormat.Type requiredFileType,
                        File file){
        dataLine = line;
        incomingStream = new AudioInputStream(line);
        audioFileType = requiredFileType;
        generatedFile = file;
    }

    public void startCapture(){
        dataLine.start();
        super.start();
    }

    public void stopCapture(){
        dataLine.stop();
        dataLine.close();
    }

    @Override
    public void run(){
        try{
            AudioSystem.write(incomingStream, audioFileType, generatedFile);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        String strFilename = "captureFile.wav";
        File outputFile = new File(strFilename);

        AudioFormat audioFormat 
                = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                                  22050.0F, 16, 2, 4, 22050.0F, false);
        DataLine.Info info = new DataLine.Info(TargetDataLine.class,
        audioFormat);
        TargetDataLine targetDataLine = null;
        try{
            targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
            targetDataLine.open(audioFormat);
        } catch (LineUnavailableException e){
            System.out.println("Unable to acquire an audio recording line");
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("AudioFormat settings: " + audioFormat.toString());
        AudioFileFormat.Type targetType = AudioFileFormat.Type.WAVE;
        AudioCapture recorder = new AudioCapture(
        targetDataLine,
        targetType,
        outputFile);

        recorder.startCapture();
        System.out.println("Starting the recording now...");
        System.out.println("Please press to stop the recording.");
        try{
            System.in.read();
        } catch (IOException e){
            e.printStackTrace();
        }

        recorder.stopCapture();
        System.out.println("Capture stopped.");
    }
}
