import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

public class AutoFiller{
	private Robot robot;
	public static final int KEY_DELAY = 100;
	public static final int WORD_DELAY = 500;

	public AutoFiller() throws Exception{
		robot = new Robot();
	}
	public static void main(String args[]) throws Exception{
		AutoFiller af = new AutoFiller();
		BufferedReader keyIn = new BufferedReader(new InputStreamReader(System.in));
		String dictionaryFile = "dictionary.txt";
		TextUntwist t = new TextUntwist(dictionaryFile);
		
		System.out.println("New Word: ");
		String tempString = keyIn.readLine();
		
		while(!"quit".equals(tempString.toLowerCase())){
			ArrayList<String> wordList = t.untwistWords(tempString);
			af.robot.delay(2000);
			af.inputList(wordList);
			System.out.println("New Word: ");
			tempString  = keyIn.readLine();
		}
		//r.delay(2000);
		//r.keyPress(
	}
	
	public void inputList(ArrayList<String> wordList){
		for(String word : wordList){
			typeWord(word);
			keyType(KeyEvent.VK_ENTER);
			robot.delay(WORD_DELAY);

		}
	}
		
	
	public void typeWord(String word){
		for(int i=0; i<word.length();i++){
			int keyCode = Character.getNumericValue(word.charAt(i)) + 55;
			keyType(keyCode);
		}
	}
	
	public void keyType(int keyStroke){
		robot.keyPress(keyStroke);
		robot.delay(KEY_DELAY);
		robot.keyRelease(keyStroke);
	}
}