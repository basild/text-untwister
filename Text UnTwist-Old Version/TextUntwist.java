/*	Program Name	:	Text Untwist
 *	Programmed By	:	Basil Dsouza
 *	Description		:	Untwists words in text twist
 */

 import java.io.*;
 import java.util.*;

 public class TextUntwist{
	public static final boolean DEBUG     = true;
	public static final int		UNTWIST   = 1;
	public static final int		ADD_WORDS = 2;
	public static final int		ANAGRAM   = 3;
	public static final int 	EXIT      = 4;

	String dictionary;

	public static void main(String args[]) throws Exception{
		String dictionaryFile = "dictionary.txt";
		TextUntwist t = new TextUntwist(dictionaryFile);

		int choice;

		do{
			choice = t.displayMenu();
		   	switch(choice){
				case UNTWIST:
					t.untwistWords();
					break;
				case ADD_WORDS:
					//addWords();
					break;
				case ANAGRAM:
					//solveAnagram();
					break;
			}
		}while(choice != EXIT);
	}

	public TextUntwist(String dict){
		dictionary = dict;
	}

	public int displayMenu() throws Exception{
		BufferedReader keyInput = new BufferedReader(new InputStreamReader(System.in));
		int choice;

		do{
			System.out.print("Text Untwister v1.0\n");
			System.out.print("\t\t                 -- By Basil Dsouza\n");
			System.out.print("Select:\n");
			System.out.print("1: Untwist\n2: Enter Data\n3: Fixed lenth Words\n");
			System.out.print("4: Exit\n-: ");
			choice = Integer.parseInt(keyInput.readLine());
		}while(choice<1 && choice>4);

		return (choice);
	}

	public void untwistWords() throws Exception{
		BufferedReader fileIn = new BufferedReader(new FileReader(dictionary));
		BufferedReader keyInput = new BufferedReader(new InputStreamReader(System.in));
		String 	word, tempString;
		char	uniqueSet[];
		int		uniqueCount[];
		int		numMatches;

		System.out.print("\t\t--- Untwisting Mode ---\n");
		System.out.print("Enter Letters: ");
		word = keyInput.readLine();

		tempString = "";
		for(int i=0; i<word.length(); i++){
			if(tempString.indexOf(word.charAt(i)) == -1){
				tempString += word.charAt(i);
			}
		}

		uniqueSet = tempString.toCharArray();
		uniqueCount = new int [uniqueSet.length];
		for(int i=0; i<tempString.length(); i++){
			uniqueCount[i] = wordCount(word, uniqueSet[i]);;
		}

		int temp;
		int i;
		int singleWords;

		tempString = fileIn.readLine();
		numMatches = 0;
		while(tempString != null){
			singleWords = 0;
			for(i=0; i<uniqueSet.length;i++){
				temp = wordCount(tempString, uniqueSet[i]);
				if(!((uniqueCount[i] >= temp))){
					break;
				}
				singleWords += temp;
			}
			if(i == uniqueSet.length && tempString.length() == singleWords){
				System.out.println(tempString);
				numMatches ++;
			}
			tempString = fileIn.readLine();
		}
		System.out.println("Total Matches: " + numMatches);
	}
	
	public ArrayList<String> untwistWords(String word) throws Exception{
		BufferedReader fileIn = new BufferedReader(new FileReader(dictionary));
		String 	tempString;
		char	uniqueSet[];
		int		uniqueCount[];
		int		numMatches;
		ArrayList<String> wordList = new ArrayList();

		tempString = "";
		for(int i=0; i<word.length(); i++){
			if(tempString.indexOf(word.charAt(i)) == -1){
				tempString += word.charAt(i);
			}
		}

		uniqueSet = tempString.toCharArray();
		uniqueCount = new int [uniqueSet.length];
		for(int i=0; i<tempString.length(); i++){
			uniqueCount[i] = wordCount(word, uniqueSet[i]);;
		}

		int temp;
		int i;
		int singleWords;

		tempString = fileIn.readLine();
		numMatches = 0;
		while(tempString != null){
			singleWords = 0;
			for(i=0; i<uniqueSet.length;i++){
				temp = wordCount(tempString, uniqueSet[i]);
				if(!((uniqueCount[i] >= temp))){
					break;
				}
				singleWords += temp;
			}
			if(i == uniqueSet.length && tempString.length() == singleWords){
				//System.out.println(tempString);
				wordList.add(tempString);
				numMatches ++;
			}
			tempString = fileIn.readLine();
		}
		return wordList;
	}
	

	public int wordCount(String toCount, char toMatch){
		int wordCounts = 0;

		for(int i=0;i<toCount.length();i++){
			if(toCount.charAt(i) == toMatch){
				wordCounts ++;
			}
		}

		return (wordCounts);
	}



int add_words()
{
   return 0;
}
/*
int all_words_there(char *word, char *curr_word)
{
   int j;
   for(j=0;j<strlen(curr_word);j++)
      if(!word_is_in(curr_word[j], word))
         return 0;

   return 1;
}


int right_amount(char *curr_word, char *unq_word, int *word_count)
{
   int i;
   for(i=0;i<strlen(unq_word);i++)
      if(word_count[i]<repeat_count(curr_word, unq_word[i]))
         return 0;

   return 1;
}

int anagram()
{
   FILE *read;
   int i,j, word_count[7], number;
   char word[8],curr_word[9], unq_word[9];
   char fname[13]="short.txt"; //Should Change before actually using
   unsigned int tens=0, units=0;

   clrscr();
   System.out.print("\t\tText Untwister v1.0\n");
   System.out.print("\t\t                 -- By Basil Dsouza\n");
   System.out.print("\t\t--- Anagram Mode ---\n");

   System.out.print("Enter Letters: ");
   scanf("%s", word);

   number=strlen(word);
   for(i=0;i<8;i++)
      unq_word[i]=0;

   j=0;
   for(i=0;i<strlen(word);i++)
      if(!word_is_in(word[i], unq_word))
         unq_word[j++]=word[i];

   unq_word[j]=0;

   for(i=0;i<strlen(unq_word);i++)
      word_count[i]=repeat_count(word, unq_word[i]);


   if(!(read=fopen(fname, "r")))
   {
      error.no=1;
      strcpy(error.desc, "wordlen0.dat_not_found");
      return (0);
   }
   units=0;
   while(!feof(read))
   {
      fscanf(read, "%s", curr_word);

      if(!all_words_there(word, curr_word)||strlen(curr_word)!=number)
         goto Leave_Loop;

//      if(!right_amount(curr_word, unq_word, word_count))
//          goto Leave_Loop;

      System.out.print("\n%s", curr_word);

Leave_Loop:
   }

   System.out.print("\n%u%u", tens,units);
   getch();
   return 1;
}
/**/
 }
