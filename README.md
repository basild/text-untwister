# Text Untiwster #

This games attempt to auto solve the text untwister game. It will unsramble words based on the selected dictionary and also auto enter that into the game using the Java Robot API

Can play online at: https://www.crazygames.com/game/text-twist

## Usage ##
![Usage](screenshots/Usage.png "Usage")

## Screenshots ##
![Main Screen](screenshots/MainScreen.png "Main Screen")

![Main Screen UnTwisting](screenshots/MainScreen-UnTwisting.png "Main Screen UnTwisting")

![Usage](screenshots/Usage.png "Usage")

![About](screenshots/AboutBox.png "About")

# Build #
This project can be built using build.xml but requires ant to be installed.
It however also contains a checked in version of the jar to make your life easier to run.