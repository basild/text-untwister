Alors Chat Client

This software is hosted at:
http://www.basildsouza.com/projects/amateur/alors-chat/

The latest documentation is at:
http://www.basildsouza.com/projects/amateur/alors-chat/documentation/

Please check license.txt accompanying this package for the usage rights 
and disclaimer.

Installation
1. Download the latest Alors Package
2. Extract to suitable location
3. Start the program by double clicking on the start.bat file 
   (in the bin dir)

Usage
A. Initiating A Conversation
   1. Sending to a new address
      When sending to address from which no messages have been sent or 
      received, the address can be typed in the main window of Alors 
      and it will initiate a conversation to the specified address.

      This can also be used with previously used addresses as well, but 
      it is generally faster to use option b in such cases.

      (Note: Alors needs to be running on both the computers between 
       which a conversation has to be setup.
            
       Alors can only check if the specified address is a valid address. 
       It does not verify that an instance of Alors is running on the 
       specified destination address. Even if no instance of Alors is 
       running on the destination computer, the conversation window will 
       open up without any error on your computer. But all messages that 
       you send will not be received by anyone.)

       Steps:
       1. Enter the IP address of the user in the "Begin Conversation" 
          text field.
       2. Click on the "Open Window" button next to the text field.
       3. A conversation window should open.

        (If the conversation window for that address is already open, 
         then it will be brought to focus)
   
   2. Sending to a previously sent address
      If messages were already sent to an address with which you have had 
      a conversation previously (since the program was started), a new 
      conversation can be initiated without having to type the address again.

      Alors stores the addresses of all the chat conversations, and makes 
      it accessible via a convenient drop down list for future use.

      Steps:
         1. Select the IP address of the user in the "Previous Conversations" 
            drop down list.
            (If the desired address is not visible, then no conversation may 
            have been initiated to that address. In which case follow Option a)
         2. Click on the "Open Window" button next to the text field.
         3. A conversation window should open.
         (If the conversation window for that address is already open, 
         then it will be brought to focus)

B. Chatting
   1. Reading messages
      Messages are read in the pane called "Received Messages" (duh!)
      
   2. Sending messages
      Messages can be sent by either:
         1. Typing the message in the "Send message" pane and pressing enter
         2. Typing the message in the "Send Message" pane and clicking on 
            the "Send" button
        (Note: To send multi-line messages, compose the message in a text 
         editor, and paste it into the chat window)

C. Troubleshooting
Program not starting
   1. Ensure you have the correct version of java, At least JRE 1.6 is required

      To verify:
         1. Type "java -version" in a command prompt window
         2. Ensure that the version says 'java version "1.6.0_xx"' 
            (Where xx can be any number)

"Internal Error: Could not start the chat client"
   1. Ensure that another instance of the chat program is not running
      Multiple instances of the application cannot run on the same port.
      If they have to be run, the port of the running instance will have 
      to be changed 
      (Though both the client and server will need to select the same port)

   2. Ensure that no other application is using port 31337
      Check that no other application is using port 31337
      This can be done via netstat 
      (However the exact process is difficult to explain here)

"Invalid Host Name: Please verify the address entered"
   1. If a host name was entered
      The hostname could not be resolved. This could be due to an improper 
      proxy configuration, or some other network peculiarity.
      
      This can be worked around by finding the ip address by some other 
      means and then using that IP address to communicate
   2. An IP address was entered
      The IP address may not be well formed. Please ensure that it is of the 
      form of xxx.xxx.xxxx.xxx where the maximum value of any of the xxx does
      not exceed 255

Messages cannot be received
   1. If neither side can receive or send messages.
      Ensure that the sender of the message is sending to the correct address.

      (Even if one side enters the correct message, Alors will open a 
      conversation window on the other side, when a message is sent)

      (Note: If neither side can receive message, then it could also mean that 
      both sides are on a NATted network. Check further troubleshooting steps 
      regarding NAT)
   
   2. If the other side can receive your messages, but you cannot receive 
      their messages.

      You may be on a NAT (Network Address Translation) network.

      To Verify:
         1. Check your local IP (Type "ipconfig" in a command prompt)
         2. Check your external IP (www.whatismyip.com)
         3. If any of the addresses displayed in step 1 match the address 
            shown in step 2, then you are not NATted 
            (and the problem lies elsewhere).
         4. If the addresses are different, then you are NATted and cannot 
            use Alors without a VPN or similar arrangement.
         5. There is also a possibility that you might not really be NATted 
            but, instead just hidden behind a router. In this case port 
            forwarding might help.
